package bendras;

public class Koordinate {
    private char x;
    private int y;

    public Koordinate(){}

    public Koordinate(char x,int y){
        this.x=x;
        this.y=y;
    }
    public void keistiKoordinate(char x,int y){
        this.x=x;
        this.y=y;
    }

    public void setKoodinate (Koordinate c){
        this.x=c.x;
        this.y=c.y;
    }
    @Override
    public String toString(){
        return ""+x+y;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Koordinate that = (Koordinate) o;

        if (x != that.x) return false;
        return y == that.y;
    }

    @Override
    public int hashCode() {
        int result = (int) x;
        result = 31 * result + y;
        return result;
    }

    public boolean close(Koordinate first, int templength) {

    }
}
