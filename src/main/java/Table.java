public class Table {

    private String name;
    private char fields[][];



    public Table(){
        this.fields = new char[10][10];
        emptySea();
    }

    public Table(String name){
        this.fields = new char[10][10];
        emptySea();
        this.name=name;

    }




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getField(int x, int y){
        return this.fields[x][y];
    }




    public void printTable (){
        for(int i=0; i<11; i++){
            for (int j = 0; j <11 ; j++) {
                if(i==0 && j==0) System.out.print(" ");
                else if (j==0) System.out.print(i-1);
                else if (i==0) System.out.print(j-1);
                else System.out.print(this.fields[i-1][j-1]);
                System.out.print('|');

            }
            System.out.println();
        }
    }

    public void emptySea(){
        for (int i = 0; i <10; i++) {
            for (int j = 0; j <10 ; j++) {

                this.fields[i][j]=' ';

            }

        }

    }

    public void addShip(int x, int y){
        this.fields[x][y]=('\u00B0');
    }



}
