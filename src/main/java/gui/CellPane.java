package gui;

import bendras.Koordinate;
import bendras.Zaidimas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CellPane extends JPanel {

    private Color defaultBackground;
    private boolean active=true;
    private Koordinate koordinate;


    public Koordinate getKoordinate() {
        return koordinate;
    }

    public void setKoordinate(Koordinate koordinate) {
        this.koordinate = koordinate;
    }



    public CellPane() {
        addMouseListener(new MouseAdapter()  {
            @Override
            public void mouseEntered(MouseEvent e) {
                if(active) {
                    defaultBackground = getBackground();
                    setBackground(Color.BLUE);
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if (active)
                setBackground(defaultBackground);
            }

            @Override
            public void mousePressed (MouseEvent e) {
                if (active) {
                    System.out.println("Mouse button pressed");
                    System.out.println("Koordinate " +koordinate );
                    Zaidimas.eventAt=koordinate;
                }
                else
                    System.out.println("Mouse button not pressed");;
            }


        });
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(30, 30);
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive(){
        return active;
    }

    public void changeColor() {
        setBackground(Color.GRAY);
  //      updateUI();
        System.out.println("Change Color");
        active=false;
    }
}
