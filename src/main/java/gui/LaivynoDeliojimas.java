package gui;

import bendras.Koordinate;
import bendras.Zaidimas;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Map;

public class LaivynoDeliojimas {
    private BattlePane manoLaivynas;

    public LaivynoDeliojimas(BattlePane mano) {
        manoLaivynas=mano;

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
        }

        JFrame frame = new JFrame("ShipBattle");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());


        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        //     panel.add(new JLabel("liko 10 laivu"));
        panel.add(new JPanel().add(new JLabel("Liko 10 laivu")));
        manoLaivynas.setBorder(new MatteBorder(30, 30, 30, 30, manoLaivynas.getBackground()));

        panel.add(manoLaivynas);

        JButton done = new JButton("done");
        done.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
            }
        });
        panel.add(done);

        //    panel.add(new JButton("done"));


        frame.add(panel);


        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);


        setSecondPoint(setFirstPoint());

    }



    private Koordinate setFirstPoint() {
        System.out.println(Zaidimas.eventAt.equals(Zaidimas.noEvent));

        while (Zaidimas.eventAt.equals(Zaidimas.noEvent)){
            Thread sleep = new Thread();
            try {
                sleep.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        Koordinate first =new Koordinate;
        first.setKoodinate(Zaidimas.eventAt);
        manoLaivynas.map.get(first).changeColor();

        Zaidimas.eventAt.setKoodinate(Zaidimas.noEvent);

        return first;






    }




    private void setSecondPoint(Koordinate first) {
        int templength=4;

        ArrayList<Koordinate> activeCells= new ArrayList<>();

        for (Map.Entry<Koordinate,CellPane> element : manoLaivynas.map.entrySet()){
            if(element.getValue().isActive()) {
                activeCells.add(element.getKey());
                if(!(element.getKey().close(first,templength)){
                    element.getValue().setActive(false);
                }
            }



        }


    }

    public class DoneButton extends JButton {

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(200, 50);
        }


    }
}


