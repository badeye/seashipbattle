package gui;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Screen {

    public Screen(BattlePane t1, BattlePane t2) {

//        new Runnable() {
//            @Override
//            public void run() {
//        EventQueue.invokeLater(() -> {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                }

                JFrame frame = new JFrame("ShipBattle");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setLayout(new BorderLayout());


                t1.setBorder(new MatteBorder(30, 30, 30, 30, Color.white));

                t2.setBorder(new MatteBorder(30, 30, 30, 30, Color.white));


                JPanel panel = new JPanel();
                panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

                panel.add(t1);
                panel.add(t2);


                frame.add(panel);


                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
//        );
    }
//}

//    public static void main(String[] args) {
//        new Screen();
//    }

//    public class TestPane extends JPanel {
//
//        public TestPane() {
//            setLayout(new GridBagLayout());
//
//            GridBagConstraints gbc = new GridBagConstraints();
//
//            for (int row = 0; row <11 ; row++) {
//                gbc.gridx = 0;
//                gbc.gridy = row;
//                LetterPanel letterpanel = new LetterPanel();
//
//                Border border = null;
//
//                if (row < 10) {
//                    border = new MatteBorder(1, 1, 0, 0, Color.GRAY);
//                } else {
//                    border = new MatteBorder(1, 1, 1, 0, Color.GRAY);
//                }
//                letterpanel.setBorder(border);
//                add(letterpanel, gbc);
//                if(row>0) letterpanel.add(new JLabel(""+row));
//
//            }
//
//            for (int col = 1; col <11 ; col++) {
//                gbc.gridx = col;
//                gbc.gridy = 0;
//                LetterPanel letterpanel = new LetterPanel();
//                Border border = null;
//
//                if (col< 10) {
//                    border = new MatteBorder(1, 1, 0, 0, Color.GRAY);
//                } else {
//                    border = new MatteBorder(1, 1, 0, 1, Color.GRAY);
//                }
//                letterpanel.setBorder(border);
//                add(letterpanel, gbc);
//                if(col>0) letterpanel.add(new JLabel("RESPUBLIKA".substring(col-1,col)));
//
//            }
//
//
//
//
//
//
//            for (int row = 1; row < 11; row++) {
//                for (int col = 1; col < 11; col++) {
//                    gbc.gridx = col;
//                    gbc.gridy = row;
//
//                    CellPane cellPane = new CellPane();
//                    Border border = null;
//                    if (row < 10) {
//                        if (col < 10) {
//                            border = new MatteBorder(1, 1, 0, 0, Color.GRAY);
//                        } else {
//                            border = new MatteBorder(1, 1, 0, 1, Color.GRAY);
//                        }
//                    } else {
//                        if (col < 10) {
//                            border = new MatteBorder(1, 1, 1, 0, Color.GRAY);
//                        } else {
//                            border = new MatteBorder(1, 1, 1, 1, Color.GRAY);
//                        }
//                    }
//                    cellPane.setBorder(border);
//                    add(cellPane, gbc);
//
//
//                }
//            }
//        }
//    }

//    public class CellPane extends JPanel {
//
//        private Color defaultBackground;
//
//        public CellPane() {
//            addMouseListener(new MouseAdapter() {
//                @Override
//                public void mouseEntered(MouseEvent e) {
//                    defaultBackground = getBackground();
//                    setBackground(Color.BLUE);
//                }
//
//                @Override
//                public void mouseExited(MouseEvent e) {
//                    setBackground(defaultBackground);
//                }
//
//                @Override
//                public void mousePressed (MouseEvent e){
//                    System.out.println("Mouse button pressed");
//                }
//
//
//            });
//        }
//
//        @Override
//        public Dimension getPreferredSize() {
//            return new Dimension(30, 30);
//        }
//    }


//    public class LetterPanel extends JPanel{
//
//        @Override
//        public Dimension getPreferredSize() {
//            return new Dimension(30, 30);
//        }
//
//    }





