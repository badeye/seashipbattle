package gui;

import bendras.Koordinate;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

public class BattlePane extends JPanel {
    public Map<Koordinate,CellPane> map =new HashMap<>();

    public BattlePane() {
        setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();

        for (int row = 0; row <11 ; row++) {
            gbc.gridx = 0;
            gbc.gridy = row;
            LetterPanel letterpanel = new LetterPanel();

            Border border = null;

            if (row < 10) {
                border = new MatteBorder(1, 1, 0, 0, Color.GRAY);
            } else {
                border = new MatteBorder(1, 1, 1, 0, Color.GRAY);
            }
            letterpanel.setBorder(border);
            add(letterpanel, gbc);
            if(row>0) letterpanel.add(new JLabel(""+row));

        }

        for (int col = 1; col <11 ; col++) {
            gbc.gridx = col;
            gbc.gridy = 0;
            LetterPanel letterpanel = new LetterPanel();
            Border border = null;

            if (col< 10) {
                border = new MatteBorder(1, 1, 0, 0, Color.GRAY);
            } else {
                border = new MatteBorder(1, 1, 0, 1, Color.GRAY);
            }
            letterpanel.setBorder(border);
            add(letterpanel, gbc);
            if(col>0) letterpanel.add(new JLabel("RESPUBLIKA".substring(col-1,col)));

        }






        for (int row = 1; row < 11; row++) {
            for (int col = 1; col < 11; col++) {
                gbc.gridx = col;
                gbc.gridy = row;

                CellPane cellPane = new CellPane();
                Koordinate koordinate=new Koordinate("RESPUBLIKA".charAt(col-1), row);
                cellPane.setKoordinate(koordinate);
                map.put(koordinate,cellPane);
                Border border = null;
                if (row < 10) {
                    if (col < 10) {
                        border = new MatteBorder(1, 1, 0, 0, Color.GRAY);
                    } else {
                        border = new MatteBorder(1, 1, 0, 1, Color.GRAY);
                    }
                } else {
                    if (col < 10) {
                        border = new MatteBorder(1, 1, 1, 0, Color.GRAY);
                    } else {
                        border = new MatteBorder(1, 1, 1, 1, Color.GRAY);
                    }
                }
                cellPane.setBorder(border);
                add(cellPane, gbc);


//                cellPane.setActive(false);



            }
        }

    }
}