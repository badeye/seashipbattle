public class TableFunctions {


    public static void printTwoTables(Table myTable, Table opponentTable){
        String spaceBetweenTables="              ";

        // Max Length of displayed name is 11
        String myName=myTable.getName();
        String opponentName=opponentTable.getName();

        myName=center(myName);
        opponentName=center(opponentName);

        System.out.println(myName+spaceBetweenTables+opponentName);


        for(int row=0; row<11; row++){

            printTableLine(myTable, row);
            System.out.print(spaceBetweenTables);
            printTableLine(opponentTable,row);

            System.out.println();
        }






    }

    private static String center (String name){

        if (name.length()>22) return name.substring(0,22).toUpperCase();

        int padWhitespacesBefore=(22-name.length())/2;
        int padWhitespaceAfter = 22- name.length()-padWhitespacesBefore;

        for (int i = 0; i <padWhitespacesBefore ; i++) name=" "+name;
        for (int i = 0; i <padWhitespaceAfter ; i++) name=name+" ";

        return name.toUpperCase();



    }


    private static void printTableLine(Table table, int row){
        String letters="ABCDEFGHIJ";
        for (int column = 0; column <11 ; column++) {
            if(row==0 && column==0) System.out.print(" ");
            else if (column==0) System.out.print(letters.charAt(row-1));
            else if (row==0) System.out.print(column-1);
            else System.out.print(table.getField(row-1,column-1));
            System.out.print('|');

        }

    }
}

