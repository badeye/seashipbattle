package persistence;

import javax.persistence.*;


@Entity
    public class Zaidejas{
        @Id
        @GeneratedValue (strategy = GenerationType.AUTO)
        private Integer id;

        @Column (name = "NAME")
        private String name;

        @Column (name = "AMZIUS")
        private int amzius;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmzius() {
        return amzius;
    }

    public void setAmzius(int amzius) {
        this.amzius = amzius;
    }
}

