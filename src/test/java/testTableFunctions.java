import org.junit.jupiter.api.Test;


public class testTableFunctions {

    @Test
    public void testPrint2Tables(){
        Table table1=new Table("test1");
        Table table2=new Table("test2");
        table1.addShip(2,2);
        table1.addShip(4,5);
        table2.addShip(7,8);

        TableFunctions.printTwoTables(table1,table2);
    }
}
